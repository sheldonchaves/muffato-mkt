package br.com.partners.gateway;

import java.util.List;

public class ListResults {

	private List<Result> results;

	public List<Result> getResults() {
		return results;
	}

	public void setResults(List<Result> results) {
		this.results = results;
	}


}


/*
'd': {
    'results': [{
        '__metadata': {
            'id': 'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='
            92585760553 ',offerId='
            0000000007 ',offerType='
            PROD ',offerKey='
            101011 ')',
            'uri': 'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='
            92585760553 ',offerId='
            0000000007 ',offerType='
            PROD ',offerKey='
            101011 ')',
            'type': 'Z_MUFFATO_OFF_REC_SRV.Ofertas'
        },
        'customerId': '92585760553',
        'offerId': '0000000007',
        'offerType': 'PROD',
        'offerKey': '101011',
        'offerRule': 'Toda família P&G com 35% de desconto',
        'offerRuleCondition': 'Para compras acima de 500 reais',
        'offerStartDate': '07/07/2017',
        'offerEndDate': '31/07/2017',
        'offerName': 'Super desconto P&G',
        'offerHeight': '2',
        'offerKeyDesc': 'HORTFG MILHO VERDE BDJ 1 UN'
    }
*/
