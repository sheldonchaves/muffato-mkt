package br.com.partners.gateway;

import java.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ServiceController {

	private String mock = "{'d':{'results':[{'__metadata':{'id':'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='92585760553',offerId='0000000007',offerType='PROD',offerKey='101011')','uri':'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='92585760553',offerId='0000000007',offerType='PROD',offerKey='101011')','type':'Z_MUFFATO_OFF_REC_SRV.Ofertas'},'customerId':'92585760553','offerId':'0000000007','offerType':'PROD','offerKey':'101011','offerRule':'Toda família P&G com 35% de desconto','offerRuleCondition':'Para compras acima de 500 reais','offerStartDate':'07/07/2017','offerEndDate':'31/07/2017','offerName':'Super desconto P&G','offerHeight':'2','offerKeyDesc':'HORTFG MILHO VERDE BDJ 1 UN'},{'__metadata':{'id':'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='92585760553',offerId='0000000007',offerType='PROD',offerKey='10771')','uri':'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='92585760553',offerId='0000000007',offerType='PROD',offerKey='10771')','type':'Z_MUFFATO_OFF_REC_SRV.Ofertas'},'customerId':'92585760553','offerId':'0000000007','offerType':'PROD','offerKey':'10771','offerRule':'Toda família P&G com 35% de desconto','offerRuleCondition':'Para compras acima de 500 reais','offerStartDate':'07/07/2017','offerEndDate':'31/07/2017','offerName':'Super desconto P&G','offerHeight':'2','offerKeyDesc':'PANIF PAO FRANCES KG'},{'__metadata':{'id':'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='92585760553',offerId='0000000008',offerType='CATE',offerKey='F%20L%20V')','uri':'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='92585760553',offerId='0000000008',offerType='CATE',offerKey='F%20L%20V')','type':'Z_MUFFATO_OFF_REC_SRV.Ofertas'},'customerId':'92585760553','offerId':'0000000008','offerType':'CATE','offerKey':'F L V','offerRule':'Compre Ameixa e Leve Batata','offerRuleCondition':'Para compras acima de 500gr de Ameixa','offerStartDate':'07/07/2017','offerEndDate':'21/07/2017','offerName':'Loucura Hortifruti','offerHeight':'20','offerKeyDesc':'F L V'}]}},{set-cookie=[sap-usercontext=sap-client=100; path=/, MYSAPSSO2=AjQxMDMBABhSAEwARQBOAFoASQAgACAAIAAgACAAIAACAAYxADAAMAADABBQAEgARAAgACAAIAAgACAABAAYMgAwADEANwAwADcAMAA2ADIAMgA0ADAABQAEAAAACAYAAlgACQACUAD%2fAPwwgfkGCSqGSIb3DQEHAqCB6zCB6AIBATELMAkGBSsOAwIaBQAwCwYJKoZIhvcNAQcBMYHIMIHFAgEBMBowDjEMMAoGA1UEAxMDUEhEAggKIBcCBSARATAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTcwNzA2MjI0MDM1WjAjBgkqhkiG9w0BCQQxFgQURqH3vL9Zq7ZxC9PEKYHeme0CPj0wCQYHKoZIzjgEAwQvMC0CFQCNkdZEYxctvp5RGSNUWY6dxMX7YgIUJlVQwejwPofutGzZ%21cLeNGff1gc%3d; path=/; domain=.168.128.175, SAP_SESSIONID_PHD_100=ER-OV0S5FIRAt22QEmImQyBVTq9inBHnqVtA8unfLfo%3d; path=/], content-type=[application/json; charset=utf-8], content-length=[2212], x-csrf-token=[WCelb383xQeg4ynvluzhYg==], dataserviceversion=[2.0], sap-metadata-last-modified=[Thu, 06 Jul 2017 17:25:18 GMT], cache-control=[no-store, no-cache], sap-processing-info=[microhub=,crp=,st=,MedCacheHub=SHM,MedCacheBEP=SHM,codeployed=X,softstate=]}";
	private String mock2 = "{'d': {'results': [{'__metadata': {'id': 'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='92585760553 ',offerId='0000000007 ',offerType='PROD ',offerKey='101011 ')', 'uri': 'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='92585760553 ',offerId='0000000007 ',offerType='PROD ',offerKey='101011 ')', 'type': 'Z_MUFFATO_OFF_REC_SRV.Ofertas'}, 'customerId': '92585760553', 'offerId': '0000000007', 'offerType': 'PROD', 'offerKey': '101011', 'offerRule': 'Toda família P&G com 35% de desconto', 'offerRuleCondition': 'Para compras acima de 500 reais', 'offerStartDate': '07/07/2017', 'offerEndDate': '31/07/2017', 'offerName': 'Super desconto P&G', 'offerHeight': '2', 'offerKeyDesc': 'HORTFG MILHO VERDE BDJ 1 UN'}, {'__metadata': {'id': 'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='92585760553 ',offerId='0000000007 ',offerType='PROD ',offerKey='10771 ')', 'uri': 'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='92585760553 ',offerId='0000000007 ',offerType='PROD ',offerKey='10771 ')', 'type': 'Z_MUFFATO_OFF_REC_SRV.Ofertas'}, 'customerId': '92585760553', 'offerId': '0000000007', 'offerType': 'PROD', 'offerKey': '10771', 'offerRule': 'Toda família P&G com 35% de desconto', 'offerRuleCondition': 'Para compras acima de 500 reais', 'offerStartDate': '07/07/2017', 'offerEndDate': '31/07/2017', 'offerName': 'Super desconto P&G', 'offerHeight': '2', 'offerKeyDesc': 'PANIF PAO FRANCES KG'}, {'__metadata': {'id': 'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='92585760553 ',offerId='0000000008 ',offerType='CATE ',offerKey='F % 20 L % 20 V ')', 'uri': 'http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/OffersSet(customerId='92585760553 ',offerId='0000000008 ',offerType='CATE ',offerKey='F % 20 L % 20 V ')', 'type': 'Z_MUFFATO_OFF_REC_SRV.Ofertas'}, 'customerId': '92585760553', 'offerId': '0000000008', 'offerType': 'CATE', 'offerKey': 'F L V', 'offerRule': 'Compre Ameixa e Leve Batata', 'offerRuleCondition': 'Para compras acima de 500gr de Ameixa', 'offerStartDate': '07/07/2017', 'offerEndDate': '21/07/2017', 'offerName': 'Loucura Hortifruti', 'offerHeight': '20', 'offerKeyDesc': 'F L V'}] } }";

	//private String baseURL = "http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_SRV/";
	private String baseURL = "http://192.168.128.175:8002/sap/opu/odata/sap/Z_MUFFATO_OFF_REC_SRV/";
	// private String service1URL = baseURL +
	// "ClienteSet('"+cpf+"')/ClientetoOfertas";

	private String token = "";
	private HttpHeaders headers;

	@RequestMapping(value = "/mock/{cpf}", method = RequestMethod.GET)
	public String getMock(@PathVariable String cpf) {
		return mock2;
	}

	@RequestMapping(value = "/client/{cpf}", method = RequestMethod.GET)
	public String getByCpf(@PathVariable String cpf) {
		return "";
	}

	public HttpHeaders getHeader() {
		String plainCreds = "rlenzi:Mela1cia";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		headers.add("Content-Type", "application/json");
		headers.add("X-CSRF-Token", "Fetch");

		/*
		 * if (token == ""){ } else { headers.add("X-CSRF-Token", token); }
		 */
		return headers;
	}

	@RequestMapping(value = "/recommendation/{cpf}", method = RequestMethod.GET)
	public @ResponseBody String getRecommendationByCpf(@PathVariable String cpf) {

		HttpEntity<String> request = new HttpEntity<String>(getHeader());

		RestTemplate restTemplate = new RestTemplate();

		String service1URL = baseURL + "ClienteSet('" + cpf + "')/ClientetoOfertas";

		Boolean TEST_MODE = false;
		
		ResponseEntity<String> recommendations;
		
		if (TEST_MODE){
			recommendations = restTemplate.exchange("https://raw.githubusercontent.com/sheldonchaves/mocs/master/mock1.json", HttpMethod.GET, request,
					new ParameterizedTypeReference<String>() {
			});
		} else {
			recommendations = restTemplate.exchange(service1URL, HttpMethod.GET, request,
					new ParameterizedTypeReference<String>() {
			});
		}

		return recommendations.getBody();

	}

}
