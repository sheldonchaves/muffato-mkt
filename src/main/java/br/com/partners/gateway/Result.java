package br.com.partners.gateway;

import java.util.List;

public class Result {

	private String __metadata;

	private String customerId;
	private String offerId;
	private String offerType;
	private String offerKey;
	private String offerRule;
	private String offerRuleCondition;
	private String offerStartDate;
	private String offerEndDate;
	private String offerName;
	private String offerHeight;
	private String offerKeyDesc;

	public String get__metadata() {
		return __metadata;
	}

	public void set__metadata(String __metadata) {
		this.__metadata = __metadata;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public String getOfferKey() {
		return offerKey;
	}

	public void setOfferKey(String offerKey) {
		this.offerKey = offerKey;
	}

	public String getOfferRule() {
		return offerRule;
	}

	public void setOfferRule(String offerRule) {
		this.offerRule = offerRule;
	}

	public String getOfferRuleCondition() {
		return offerRuleCondition;
	}

	public void setOfferRuleCondition(String offerRuleCondition) {
		this.offerRuleCondition = offerRuleCondition;
	}

	public String getOfferStartDate() {
		return offerStartDate;
	}

	public void setOfferStartDate(String offerStartDate) {
		this.offerStartDate = offerStartDate;
	}

	public String getOfferEndDate() {
		return offerEndDate;
	}

	public void setOfferEndDate(String offerEndDate) {
		this.offerEndDate = offerEndDate;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getOfferHeight() {
		return offerHeight;
	}

	public void setOfferHeight(String offerHeight) {
		this.offerHeight = offerHeight;
	}

	public String getOfferKeyDesc() {
		return offerKeyDesc;
	}

	public void setOfferKeyDesc(String offerKeyDesc) {
		this.offerKeyDesc = offerKeyDesc;
	}

}
