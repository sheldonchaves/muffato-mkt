var app = angular.module('muffato', []);

app.controller('mktCtr', function($scope, $http) {
	$scope.title = 'Recomendação de Ofertas';
	$scope.recommendations = [];
	$scope.cpf = '';

	function processRecommendations(result) {
		angular.forEach(result, function(value, key) {
			var checkItem = checkExist(value.offerId);
			
			if (checkItem){
				checkItem.items.push(value);
			} else {
				var recommendation = {
					offerId: value.offerId,
					open: false,
					items: [value]
				};
				$scope.recommendations.push(recommendation);
			}
		});
	}
	
	
/*    "offerId": "0000000007",
    "offerType": "PROD",
    "offerKey": "101011",
    "offerRule": "Toda família P&G com 35% de desconto",
    "offerRuleCondition": "Para compras acima de 500 reais",
    "offerStartDate": "07/07/2017",
    "offerEndDate": "31/07/2017",
    "offerTargetLink":"http://delivery.supermuffato.com.br/busca/?fq=H:888",
    "offerImageLink":"http://muffatosupermercados.vteximg.com.br/arquivos/smd_home_17_07_03_topo_peg_35off.jpg",
    "offerName": "Super desconto P&G",
    "offerHeight": "2",
    "offerKeyDesc": "HORTFG MILHO VERDE BDJ 1 UN"
*/

	$scope.openDetail = function(recommendation) {
		recommendation.open = !recommendation.open;
	}
	
	$scope.search = function() {
		$http.get('/recommendation/' + $scope.cpf).then(function(responce) {
			processRecommendations(responce.data.d.results);
		});
	}
	
	$scope.search();

	function checkExist(id) {
		var returnValue = '';
		angular.forEach($scope.recommendations, function(value, key) {
			if (id == value.offerId){
				returnValue = value;
			}
		});
		return returnValue;
	}
});
